import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";
//Scenario 2
//test 1 fullføre riktig kjøp
Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
  cy.visit("http://localhost:8080");
  cy.get("#product").select("Hubba bubba");
  cy.get("#quantity").clear().type("4");
  cy.get("#saveItem").click();

  cy.get("#product").select("Smørbukk");
  cy.get("#quantity").clear().type("5");
  cy.get("#saveItem").click();

  cy.get("#product").select("Stratos");
  cy.get("#quantity").clear().type("1");
  cy.get("#saveItem").click();

  cy.get("#product").select("Hobby");
  cy.get("#quantity").clear().type("2");
  cy.get("#saveItem").click();
});

And(/^trykket på Gå til betaling$/, () => {
  cy.get("#goToPayment").click();
});

When(
  /^jeg legger inn navn, adresse, postnummer, poststed og kortnummer$/,
  () => {
    cy.get("#fullName").clear().type("Ola Nordmann");
    cy.get("#address").clear().type("Adresseveien 13");
    cy.get("#postCode").clear().type("7012");
    cy.get("#city").clear().type("Trondher");
    cy.get("#creditCardNo").clear().type("0123456789876543");
  }
);

And(/^trykker på fullfør kjøp$/, () => {
  cy.get('input[type="submit"]').should("have.value", "Fullfør handel").click();
});

Then(/^skal jeg få beskjed om at kjøpet er registrert$/, () => {
  cy.get("div.confirmation").should("contain.text", "Din ordre er registrert.");
});

//Test 2 av ugyldig informasjon
Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
  cy.visit("http://localhost:8080");
  cy.get("#product").select("Hubba bubba");
  cy.get("#quantity").clear().type("4");
  cy.get("#saveItem").click();

  cy.get("#product").select("Smørbukk");
  cy.get("#quantity").clear().type("5");
  cy.get("#saveItem").click();

  cy.get("#product").select("Stratos");
  cy.get("#quantity").clear().type("1");
  cy.get("#saveItem").click();

  cy.get("#product").select("Hobby");
  cy.get("#quantity").clear().type("2");
  cy.get("#saveItem").click();
});

And(/^trykket på Gå til betaling$/, () => {
  cy.get("#goToPayment").click();
});

When(/^jeg legger inn ugyldige verdier i disse feltene$/, () => {
  cy.get("#fullName").clear().blur();
  cy.get("#address").clear().blur();
  cy.get("#postCode").clear().blur();
  cy.get("#city").clear().blur();
  cy.get("#creditCardNo").clear().blur();
});

Then(/^skal jeg få feilmeldinger for disse$/, () => {
  cy.get("#fullNameError").should("contain.text", "Feltet må ha en verdi");
  cy.get("#addressError").should("contain.text", "Feltet må ha en verdi");
  cy.get("#postCodeError").should("contain.text", "Feltet må ha en verdi");
  cy.get("#cityError").should("contain.text", "Feltet må ha en verdi");
  cy.get("#creditCardNoError").should(
    "contain.text",
    "Kredittkortnummeret må bestå av 16 siffer"
  );
});
