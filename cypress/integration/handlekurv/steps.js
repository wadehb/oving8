import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";
//Scenario 1
//test 1 for om alle tingene blir lagt til og har riktig pris
Given(/^at jeg har åpnet nettkiosken$/, () => {
  cy.visit("http://localhost:8080");
});

When(/^jeg legger inn varer og kvanta$/, () => {
  cy.get("#product").select("Hubba bubba");
  cy.get("#quantity").clear().type("4");
  cy.get("#saveItem").click();

  cy.get("#product").select("Smørbukk");
  cy.get("#quantity").clear().type("5");
  cy.get("#saveItem").click();

  cy.get("#product").select("Stratos");
  cy.get("#quantity").clear().type("1");
  cy.get("#saveItem").click();

  cy.get("#product").select("Hobby");
  cy.get("#quantity").clear().type("2");
  cy.get("#saveItem").click();
});

Then(/^skal handlekurven inneholde det jeg har lagt inn$/, () => {
  [/4 Hubba bubba/, /5 Smørbukk/, /1 Stratos/, /2 Hobby/].forEach((re) =>
    cy.get("#list").contains(re)
  );
});

And(/^den skal ha riktig totalpris$/, function () {
  cy.get("#price").should("have.text", "33");
});

//Test 2 for å fjerne
Given(/^at jeg har åpnet nettkiosken$/, () => {
  cy.visit("http://localhost:8080");
});

And(/^lagt inn varer og kvanta$/, () => {
  cy.get("#product").select("Hubba bubba");
  cy.get("#quantity").clear().type("2");
  cy.get("#saveItem").click();

  cy.get("#product").select("Smørbukk");
  cy.get("#quantity").clear().type("5");
  cy.get("#saveItem").click();

  cy.get("#product").select("Stratos");
  cy.get("#quantity").clear().type("1");
  cy.get("#saveItem").click();

  cy.get("#product").select("Hobby");
  cy.get("#quantity").clear().type("2");
  cy.get("#saveItem").click();
});

When(/^jeg sletter varer$/, () => {
  cy.get("#product").select("Hubba bubba");
  cy.get("#deleteItem").click();

  cy.get("#product").select("Smørbukk");
  cy.get("#deleteItem").click();

  cy.get("#product").select("Stratos");
  cy.get("#deleteItem").click();
});

Then(/^skal ikke handlekurven inneholde det jeg har slettet$/, () => {
  cy.get("#list").contains("Hobby");
  cy.get("#list").children().should("have.length", 1);
});

//Test 3 for å bytte kvanta
Given(/^at jeg har åpnet nettkiosken$/, () => {
  cy.visit("http://localhost:8080");
});

And(/^lagt inn varer og kvanta$/, () => {
  cy.get("#product").select("Hubba bubba");
  cy.get("#quantity").clear().type("2");
  cy.get("#saveItem").click();

  cy.get("#product").select("Smørbukk");
  cy.get("#quantity").clear().type("5");
  cy.get("#saveItem").click();

  cy.get("#product").select("Stratos");
  cy.get("#quantity").clear().type("1");
  cy.get("#saveItem").click();

  cy.get("#product").select("Hobby");
  cy.get("#quantity").clear().type("2");
  cy.get("#saveItem").click();
});

When(/^jeg oppdaterer kvanta for en vare$/, () => {
  cy.get("#product").select("Stratos");
  cy.get("#quantity").clear().type("3");
  cy.get("#saveItem").click();
});

Then(/^skal handlekurven inneholde riktig kvanta for varen$/, function () {
  cy.get("#price").should("have.text", "45");
  cy.get("#list").contains("3 Stratos");
});
